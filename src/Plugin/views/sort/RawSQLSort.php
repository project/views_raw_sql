<?php

namespace Drupal\views_raw_sql\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default implementation of a raw SQL sort plugin.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("sort_views_raw_sql")
 */
class RawSQLSort extends SortPluginBase {

  /**
   * Provides current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * Cannot be exposed.
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $token_service = \Drupal::token();
    $sql = $token_service->replace($this->options['raw_sql']);
    $this->query->addOrderBy(NULL, $sql, $this->options['order'], $this->options['id']);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['raw_sql'] = ['default' => 0];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if ($this->account->hasPermission('edit views raw sql')) {
      $form['raw_sql'] = [
        '#type' => 'textarea',
        '#title' => t('Raw SQL'),
        '#default_value' => $this->options['raw_sql'],
        '#weight' => -6,
        '#required' => TRUE,
      ];
    }
  }

}
