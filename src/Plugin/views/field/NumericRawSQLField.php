<?php

namespace Drupal\views_raw_sql\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Plugin\views\field\NumericField;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 * @ViewsField("field_views_raw_sql_numeric")
 */
class NumericRawSQLField extends NumericField implements ContainerFactoryPluginInterface {

  /**
   * Provides current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * Sets the initial field data at zero.
   */
  public function query() {
    $this->ensureMyTable();
    // Add the field.
    $group_type = $this->options['my_group_type'];
    $params = $group_type != 'group' ? ['function' => $group_type] : [];

    $token_service = \Drupal::token();
    $sql = $token_service->replace($this->options['raw_sql']);
    $this->field_alias = $this->query->addField(NULL, $sql, 'raw_sql_field', $params);
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['raw_sql'] = ['default' => 0];
    $options['my_group_type'] = ['default' => 'sum'];
    return $options;
  }

  /**
   * Provide a form for aggregation settings.
   */
  public function buildGroupByForm(&$form, FormStateInterface $form_state) {
    parent::buildGroupByForm($form, $form_state);
    $options = [];
    foreach ($this->query->getAggregationInfo() as $key => $value) {
      $options[$key] = $value['title'];
    }

    unset($form['group_type']);

    $form['my_group_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Aggregation type'),
      '#description' => $this->t("Change this setting in the field configutation form."),
      '#options' => $options,
      '#parent' => NULL,
      '#default_value' => $this->options['my_group_type'],
    ];
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   *
   * There is no need for this function to actually store the data.
   */
  public function submitGroupByForm(&$form, FormStateInterface $form_state) {
    parent::submitGroupByForm($form, $form_state);
    $item = &$form_state->get('handler')->options;

    $item['my_group_type'] = $form_state->getValue(['options', 'my_group_type']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if ($this->account->hasPermission('edit views raw sql')) {
      $form['raw_sql'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Raw SQL'),
        '#default_value' => $this->options['raw_sql'],
        '#weight' => -6,
        '#required' => TRUE,
      ];
    }
  }

}
