CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Views Raw SQL gives designated users the ability to add raw SQL to a view.
This means that tha field can be produced from the results of an SQL statement,
or a filter or sort can be applied based on the result of an SQL statement.

REQUIREMENTS
------------

The Views Raw SQL module has no requirements outside the Core distribution.

INSTALLATION
------------

Install the Views Raw SQL module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420
for further information.


CONFIGURATION
-------------

The module supports fields, filters, sorts and arguments. The Views Raw SQL permission
is required to edit a Raw SQL string. Anyone can create or delete these items.

In general, there is a text field for entering the SQL expression. This must be
a valid SQL expression or the view will fail. There is no checking done by the module.
The text field can contain global tokens like [site:name] which will be replaced
with the appropriate value.

The sort element will add a new field to the result.

The argument element has an additional token, [argument].

MAINTAINERS
-----------

 * Beakerboy - https://www.drupal.org/u/beakerboy
